# Projektowanie oprogramowania: zadania_10 (tablice, stringi)
## Zadanie jest obowiązkowe

***
Lista zadań:
- [ ] Zadanie_0: Sprawdzanie pary.
- [ ] Zadanie_1: Przedział.
- [ ] Zadanie_2: Czy suma liter jest parzysta czy nieparzysta?
- [ ] Zadanie_3: Sortuj według częstotliwości.
- [ ] Zadanie_4: Zmień kolejność cyfr.

***

Każde zadnaie zrealizuj w osobnej przestrzeni nazw.

***

## Zadanie_0 - szczegóły
- podczas implementacji, jeśli będziesz uważał, aby dodać dodatkową funkcję, zrób to w przestrzeni nazw,
- funkcja `execute` przyjmuje 2 parametry:
    - tablica dynamiczna,
    - wartość,
- zadanie polega na znalezieniu pary liczb z tablicy tak, aby ich iloczyn był równy `wartości` podanej jako 2 parametr,
- wyświetl odpowiedni komunikat na ekranie,
- zacommituj zmiany.

## Zadanie_1 - szczegóły
- podczas implementacji, jeśli będziesz uważał, aby dodać dodatkową funkcję, zrób to w przestrzeni nazw,
- funkcja `execute` przyjmuje 1 parametr:
    - tablica dynamiczna,
- w matematyce interwał jest różnicą między największą i najmniejszą liczbą w tablicy,
- zadanie polega na znalezieniu największe i najmniejszej liczby, obliczenie różnicy między nimi oraz wyświetlenie ":)" jeśli przedział tablicy jest równy jakiemukolwiek innemu elementowi; w przeciwnym razie zwraca ":(",
- wyświetl odpowiedni komunikat na ekranie,
- zacommituj zmiany.

## Zadanie_2 - szczegóły
- podczas implementacji, jeśli będziesz uważał, aby dodać dodatkową funkcję, zrób to w przestrzeni nazw,
- funkcja `execute` przyjmuje jeden parametr:
    - ciąg znaków (std::string),
- nierozróżnianie wielkości liter -> czyli litera `A` lub `a` jest traktowana jako 1, a litera `Z` lub `z` 26,
- ignorowanie symboli innych niż literowe,
- zadanie polega na obliczeniu sumy wszystkich liter oraz określenie czy obliczona suma jest parzysta lub nieprzysta
- wyświetl odpowiedni komunikat na ekranie,
- zacommituj zmiany.

## Zadanie_3 - szczegóły
- podczas implementacji, jeśli będziesz uważał, aby dodać dodatkową funkcję, zrób to w przestrzeni nazw,
- funkcja `execute` przyjmuje jeden parametr:
	- tablica dynamiczna
- zadanie polega na posortowaniu elementów tablicy od najmniejszego do najwiekszego,
- wyświetl odpowiedni komunikat na ekranie przed sortowaniem oraz po sortowaniu,
- zacommituj zmiany.

## Zadanie_4 - szczegóły
- podczas implementacji, jeśli będziesz uważał, aby dodać dodatkową funkcję, zrób to w przestrzeni nazw,
- funkcja `execute` przyjmuje 2 parametry:
    - dynamiczna tablica,
    - ciag znaków (std::string), parametr, który informuje jak mają zostać posortowane cyfry, które znajdują się w liczbie
- zadanie polega na zmienia kolejność cyfr każdego elementu numerycznego w tablicy na rosnącą (asc) lub malejącą (desc),
- wyświetl odpowiedni komunikat na ekranie przed sortowaniem oraz po sortowaniu,
- zacommituj zmiany.
