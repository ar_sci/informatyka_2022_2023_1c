# Informatyka: zadania_05

## Zadania
```
Napisz program w języku programowania C++:

- [ ] Zadanie_0: Zaimplementuj funkcję: addition.
- [ ] Zadanie_1: Zaimplementuj funkcję: subtraction.
- [ ] Zadanie_2: Zaimplementuj funkcję: multiplication.
- [ ] Zadanie_3: Zaimplementuj funkcję: division.
- [ ] Zadanie_4: Zaimplementuj funkcję: modulo.
- [ ] Zadanie_5: Zaimplementuj funkcję: menu.
```

***

## Zadanie: 0, 1, 2, 3, 4 - szczegóły
- Zaimplementuj funkcję w języku c++
- Funkcja musi składać się z deklaracji i definicji.
- Operuj na bazowym typie: `float`, a funckja `modulo` musi operować na typie `int`.
- Funkcja musi posiadać dwa argumenty.
- Funkcja musi posiadać zmienną wynik, do której przypiszesz wynik z obliczeń
- Funkcja musi zwracać wynik za pomocą słowa kluczowego: `return`
- Przetestuj zaimplementowane funkcje w funkcji `main`:
    ``` c++
    std::cout << addition(1.0f, 2.0f) << std::endl;
    std::cout << subtraction(5.0f, 2.0f) << std::endl;
    std::cout << multiplication(2.0f, 2.0f) << std::endl;
    std::cout << division(3.0f, 2.0f) << std::endl;
    std::cout << modulo(5, 2) << std::endl;
    ```

## Zadanie: 5 - szczegóły
- Zaimplementuj funkcję, która będzie wyświetlać następujące informacje:
    ```
    1. Dodawanie
    2. Odejmowanie,
    3. Mnozenie
    4. Dzielenie
    5. Dzielenie z reszta

    0. Wyjscie
    ```
- Wykorzystaj tutaj strumień `std::cout`, aby wyświetlić napis na konsoli systemu operacyjnego
- Ta funkcja nie będzie zwracać żadnego wyniku, więc można wykorzystać tutaj typ `void`
- Przetestuj zaimplementowane funkcje w funkcji `main`:
    ``` c++
    menu();
    ```
    