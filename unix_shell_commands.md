# Podstawowe komendy w powłoce UNIX

- `ls -al` => wypisuje wszystkie katalogi oraz pliki w określonym katalogu (domyślnie jest to obecny katalog)
    - parametr `a` => wypisuje również ukryte pliki (czyli te których nazwa zaczyna się kropką)
    - parametr `l` => szczegółowa lista

- `cd {nazwa_katalogu}` => change directory => zmienia aktualny katalog
    - `cd ..` => przechodzi do katalogu o jeden wyższego w drzewie katalogów niż obecny
    - `cd ~` => przechodzi do katalogu domowego
    - `cd dir1/dir2/dir3` => można wprowadzić całą ścieżkę
    - `cd ../dir2/../../dir5` => można dobrowolnie mieszać przechodzenie między katalogami

- `pwd` => `print working directory` =>  wypisuje ścieżkę obecnego katalogu

- `cat {nazwa_pliku}` => wyświetla zawartość pliku w terminalu
    - pamiętaj, że możesz podać kilka plików, separatorem jest spacja

- `tac {nazwa_pliku}` => wyświetla zawartość pliku w terminalu w odwrotnej kolejności niż `cat`

- `echo` =>  powtarza na standardowym wyjściu słowa podane w argumencie

- `wc {nazwa_pliku}` => program do zliczania linii, słów, znaków
    - parametr `l` => zlicza tylko linie
    - parametr `w` => zlicza tylko słowa
    - parametr `c` => zlicza tylko znaki
    - można wprowadzić kilka plików, wtedy wyświetli dane dla pliku oraz pokaże zsumowane wartości

- `mkdir {nazwa_katalogu}` => tworzy katalog
- `rmdir {nazwa_katalogu}` => kasuje katalog

- `touch {nazwa_pliku}` => tworzy plik
- `rm {nazwa_pliku}` => kasuje plik

- `cp {źródło} {miejsce}` => kopiuje plik 
    - `cp plik1.txt plik.copy.txt`
    - można podawać ścieżki do plików zawierające katalogi

- `mv {źródło} {miejsce}` => przenosi plik, używane też jako narzędzie do zmiany nazwy pliku
    - można podawać ścieżki do plików zawierające katalogi

---

- `>` => przekierowanie wyjścia z programu do pliku
- `>>`=> przekierowanie wyjścia z programu do pliku, lecz dodaje je na koniec pliku
- `|` => przekierowanie wyjścia z programu do kolejnego programu
- `>!` => przekierowanie do pliku. Działa podobnie jak `>`, ale kontynuuje nawet gdy plik już istnienie
