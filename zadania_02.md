1. Utwórz nowy projekt w `Visual Studio` dla języka programownia `c++`.
1. Utwórz zmienną liczbową, przypisz do niej wartość `0`.
1. Wyświetl komunikat dla użytkownika:
    ```
    Wybierz opcję programu:
    1. Dodawanie
    2. Odejmowanie,
    3. Mnozenie
    4. Dzielenie
    5. Dzielenie z reszta

    0. Wyjscie
    ```
1. Umożliwij wprowadzenie wartośći do programu poprzez konsolę.
1. Utwórz dwie zmienne, które będą przechowywały komponenty potrzebne do wykonania działania.
1. Utwórz zmienną przechowującą wynik działania
1. Za pomocą warunków `if`, sprawdź wprowadzoną opcję przez użytkownika.
1. Dla każdego warunku wykonaj działanie arytmetyczne.
1. Wyświetl wynik.
1. Zakończ działanie programu.
