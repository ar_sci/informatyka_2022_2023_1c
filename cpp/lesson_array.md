```c++
#include <iostream>

int main(int argc, char* argv[])
{
    srand(38122);

    int x = 0;
    // int 
    // float
    // bool <=true false

    // true > !0
    // false === 0

    // ==
    // != 
    // <
    // <=
    // >=

    if (true) {
        // instrukcje
    }

    const int tab_size = 100;
    int tab[tab_size] = {};

    for (int i = 0; i < tab_size; ++i) {
        // 0 > 32766
        //tab[i] = rand() / 32766.0f;
        //tab[i] = rand() % 101;

        int min = 35;
        int max = 50;
        tab[i] = min + rand() % (max - min);
        std::cout << "index: " << i << " " << "value: " << tab[i] << std::endl;
    }

    return 0;
}

```
