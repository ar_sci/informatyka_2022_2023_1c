# Pętla for

Składnia:
- `for (iterator; warunek; skok) {}`
- iterator (na początku będzie to indeks)
- warunek, sprawdzenie iteratora czy spełnia określone założenie,
- skok, definiujemy co ile jednostek iterator ma zostać przesunięty

```c++
const unsigned int size = 10;
for (unsigned int i = 0; i < size; i++) {
    std::cout << i + 1 << ". Nie będę przeklinać na języku niemieckim." << std::endl;
}
```
