# Kod źródłowy:
```c++
#include <iostream>

// deklaracja funkcja sklada sie z nastepujacych elementow:
// - typ zwracanej funkcji
// - nazwa fukcji
// - argumenty funkcji

// deklaracja funkcji:
// funkcja zwraca wartosc o typie int
// funkcja nazywa sie funkcja
// funkcja posiada 3 argumenty
int funkcja(int argument_1, float argument_2, char argument_3);

// deklaracja funkcji:
// funkcja niczego nie zwaraca (posiada typ void)
// funkcja nazywa sie foo
// funkcja posiada 1 argument
void foo(int wartosc);

// deklaracja funkcji nie musi miec sprecyzowanych nazw argumentow
void test(int, float, char, int);

// deklaracja funkcji
// int      <= typ elementu, któa funkcja zwraca
// funkcja  <= nazwa funkcji
// ()       <= deklarowanie typów argumentów
int funkcja2(int argument_1, int argument_2);

// void     <= typ, który oznacza, że funckja nie będzie posiadać wyniku
// funkcja nie posiada argumentów
void foo2();
void foo3(int a);

// funkcja main
int main(int argc, char* argv[])
{
    // wywolanie funkcji odbywa sie przy wykorzystaniu nazwy funkcji oraz podaniu odopowiednich parametrow do funkcji:
    foo(5);

    // jesli funkcja zwraca wynik, to mozesz przypisac wynik do zmiennej, ale tez nie musisz tego robic :)
    int tmp = funkcja(1, 2.0f, 'c');

    // wywołanie funkcji
    // wiemy ze funkcja zwraca jakiś element typu int
    // nie musimy zawsze przypisywać wyniku do zmiennej, aby wywołać funkcję
    funkcja2(5, 4);

    // inicjalizacja zmiennej z przypisaniem wynikiem wywołanej funkcji
    int wynik = funkcja2(5, 4);

    // do funkcji również możemy przekazywać zmienne;
    int a = 4 + 5 + 6;
    int b = 99 - 23 * 2;
    int wynik2 = funkcja2(a, b);
    int wynik3 = funkcja2(wynik2, b);

    // również wywołanie funkcji możemy przypisać jako parametr funckji
    // co sprawi, że funckje zostaną wyliczone w następujący sposób
    // tymczasowy_wynik_1 = funkcja(3, 4)
    // tymczasowy_wynik_1 = 7;
    // tymczasowy_wynik_2 = funkcja(a, b)
    // tymczasowy_wynik_2 = 68;
    // wynik4 = funkcja(68, 7);
    // wynik4 = 75;
    int wynik4 = funkcja2(funkcja2(a, b), funkcja2(3, 4));


    // wywołanie funkcji
    foo2();
    foo3(5);
    foo3(funkcja2(5, 10));

    return 0;
}

// definicja funkcji:
// posiada wszystkie elementy z deklaracji oraz dodatkowo cialo funkcji
int funkcja(int argument_1, float argument_2, char argument_3)
{
    // funkcja musi zwracac jaki wynik, poniewaz zdefiniowalismy int:
    return 1000;
}

void foo(int wartosc)
{
    // funkcja void, nie musi posiadac slowa kluczowego return
    std::cout << "wartosc wynosi: " << wartosc << std::endl;
}

// rowniez w deklaracji nie musicie podawac nazw argumentow, no chyba ze bedzie z ktoregos korzystac, to juz musicie podac
void test(int, float, char, int)
{
}

// definicja funkcji
// składa się z tych samych elementów co deklaracja
// tylko dopisuje się ciało funkcji w { }
// funckja ma określony typ, więc musi zwracać wartość za pomocą słowa kluczowego return
int funkcja2(int argument_1, int argument_2)
{
    // przykładowe wykorzystanie argumentów w funkcji
    // jest to suma dwóch liczb
    return argument_1 + argument_2;
}

// definicja funkcji
void foo2()
{
    int x = 5;

    // w funkcjach można używać słówka kluczowego return
    // oznacza to, ze w tym miejscu zakończy się działanie funkcji
    return;

    // nigdy to sie nie wykona, poniewaz return zakonczy działanie funkcji
    x = 10;
}

// definicja funckji
void foo3(int a)
{
    std::cout << a << std::endl;
}
```