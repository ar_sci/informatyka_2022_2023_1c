## Bazowe typy
| Typ               | rozmiar (bity)    | wartości          |
| :---              | :---:             | :---:             |
| *void*            | -                 | -                 |
| *bool*            | 8                 | true / false      |
| *char*            | 8                 | -128 ; 127        |
| *float*           | 32                |                   |
| *double*          | 64                |                   |
| *short*           | 16                | -32768 ; 32767    |
| *int*             | 32                | -2,147,483,648 ; 2,147,483,647    |
| *long long int*   | 64                | -9,223,372,036,854,775,808 ; 9,223,372,036,854,775,807    |

## Modyfikatory
| Typ       |       |
| :---      | :---: |
| signed    | - / + |
| unsigned  | +     |

| Przykład          | rozmiar   | zakres    |
| :---              | :---:     | :---:     |
| *unsigned char*   | 8         | 0 ; 255   |

## Zmienna
W języku programowania, aby posługiwać się zmiennymi należy wykorzystać schemat:
```
typ_danych nazwa_zmiennej;
```

Inicjalizacja jest to nadawanie początkowej wartości zmiennej w chwili jej tworzenia:
```c++
{
    int zmienna1 = 5;
    int zmienna2(5);
    int zmienna3{5};
}
```

Przypisywanie wartości do zmiennej:
```c++
{
    int zmienna = 5;    // inicjalizacja
    zmienna = 10;       // przypisanie
}
```

## Komentarze
Jedna linia, wykorzystanie `//`:
```c++
// komentarz jednej linii
```

Wiele linii, wykorzystanie `/**/`:
```c++
/*
    komentarz, który zawiera kilka linii,
    druga linia,
    trzecia linia
*/
```

## Zródła
- [cppreference](https://en.cppreference.com)
- [cpp0x.pl](http://cpp0x.pl/)
