```c++
#include <iostream>

int main(int argc, char* argv[])
{
    int liczba_0 = -1;
    float liczba_1 = 0.1234f;
    bool flag = true; // flase

    // + - * / %
    // = -> przypisanie

    //  lhs  ==  rhs
    //       != 
    //        <
    //        >
    //       <=
    //       >=


    float a = 0.0f;
    float b = 0.0f;
    std::cin >> a;
    std::cin >> b;
    float wynik = 0.0f;

    //bool flag = true; // flase
    if (b != 0.0f) {
        wynik = a / b;
        std::cout << wynik << std::endl;
    }
    else {
        std::cout << "Nie dziel przez 0." << std::endl;
    }

    bool test_is_not_zero = b != 0.0f;
    if (test_is_not_zero) {
        wynik = a / b;
        std::cout << wynik << std::endl;
    }

    //if (wynik == 15.0f + 18.0f) {
    //}

    return 0;
}
```
