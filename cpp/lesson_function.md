# Funkcje
Schemat budowania funkcji:
```
typ_zwracanej_wartosci nazwa_funkcji(typ_argumentu_1 argument_1, typ_argumentu_2 argument_2)
{
    return zwracana_wartosc;
}
```

`typ_zwracanej_wartości` => jest to typ, który informuje nas, co nam funkcja zwróci,
wykorzystanie typu `void`, spowoduje że funkcja nie musi niczego zwracać

`nazwa_funkcji` => jest to nazwa funckji

`()` => funkcja zawiera nawiasy okrągłe, w których znajdują się argumenty,
tak jak powyższym przykładzie, funkcja posiada dwa argumenty,
pierwszy argument jest typu int, a drugi typu float.

`retrun` => słowo kluczowe, które kończy działanie funkcji (może zwracać wartość lub nie)

`{}` => ciało funkcji

---

Przykład w języku programowania c++:
```c++
int funkcja(int argument_1, float argument_2)
{
    return 0;
}
```

## Deklaracja, a definicja
Przyklad:
```c++
// deklaracja
int funkcja();

int main(int argc, char* argv[]) {}

// definicja
int funkcja()
{
    return 0;
}
```

## Zródła
- [cppreference](https://en.cppreference.com)
- [cpp0x.pl](http://cpp0x.pl)
