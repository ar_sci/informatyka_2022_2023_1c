# Projektowanie oprogramowania: `std::string`

`std::string` jest to złożony typ danych, który przechowuje oraz manipuluje ciągiem znakiem. Wcześniej korzystałeś z bazowego typu `char`, który później przekształcony w tablicę o stałym lub dynamicznym rozmiarze. Musiałeś dbać o alokację oraz kopiowanie danych, lecz przy wykorzystaniu tego typu, zmieni się twoje podejście do obsługi danych tekstowych.

Przykład wykorzystania typu `std::string`:
```c++
// utworzenie zmiennej oraz przypisanie tekstu do zmiennej
std::string tekst = "To jest tekst";

// wyświetlenie zawartości
std::cout << tekst << std::endl;

// wprowadzenie zawartości
// std::cin przekaże liczbę, tekst do spacji
// wprowadzenie następującego tekstu: ToJestTekst 123 392
// zmienna `tekst` będzie przechowywać tylko ten element: `ToJestTekst`
// należy pamiętać, że każda informacja jest przechowywana jako tekst
std::cin >> tekst;
```

---

`std::string` jest to złożony typ danych, czyli klasa, która posiada wbudowane metody (mogą modyfikować lub przetwarzać tekst w celu osiągnięcia pożądanego żądania)

---

Metody, które mogą zostać wykonane na zmiennej (te najbardziej potrzebne):
- `.size()` lub `.length()` => metoda zwraca rozmiar tekstu \
    ```c++
    std::string tekst;
    std::cin >> tekst;
    unsigned int dlugosc = tekst.size();
    std::cout << "Dlugosc tekstu wynosi: " << dlugosc << std::endl;
    ```
- `.clear()` => metoda usuwa tekst z zmiennej
    ```c++
    std::string tekst = "To jest tekst";
    std::cout << "Tekst: " << tekst << std::endl;
    tekst.clear();
    std::cout << "Tekst: " << tekst << std::endl;
    ```
- `.empty()` => metoda zwraca `true(1)` jeśli tekst jest pusty, inaczej `false(0)`
    ```c++
    std::string tekst = "To jest tekst";
    bool test = tekst.empty();
    std::cout << "is_empty: " << test  << std::endl;
    tekst.clear();
    test = tekst.empty();
    std::cout << "is_empty: " << test << std::endl;
    ```
- `.find({szukany ciąg znaków}, {opcjonalne: od jakiego miejsca ma szukać})` => szuka zadanego ciągu znaków od początku lub zadanego miejsca w ciągu znaków
    ```c++
    std::string tekst = "To jest tekst jest";
    unsigned int miejsce = tekst.find("jest");
    std::cout << "Pierwsze slowo `jest` znajduje sie: " << miejsce  << std::endl;
    // do poprzedniego indeksu dodajemy wartość 1, aby nie wyszukiwać już podobnego ciągu znaków 
    miejsce = tekst.find("jest", miejsce + 1);
    std::cout << "Drugie slowo `jest` znajduje sie: " << miejsce  << std::endl;
    miejsce = tekst.find("jest", miejsce + 1);
    std::cout << "Co jeśli nie znajdzie?? wartość to: " << miejsce  << std::endl;
    ```
    - wartość, która została wyświetlona to specjalna zmienna `std::string::npos`, dzieki niej, wiemy, że nie znajdujemy elementu w ciągu znaków (w typie std::string)
- `.substr({opcjonalne: start cięcia}, {opcjonalne: ile znaków})` => wycina element tekstu oraz tworzy jego kopię
    ```c++
    std::string tekst = "To jest tekst jest";
    std::string kawalek1 = tekst.substr(4);
    std::cout << kawalek1 << std::endl;
    std::string kawalek2 = tekst.substr(4, 5);
    std::cout << kawalek2 << std::endl;
    ```

---

Operatory, które mogą zostać wykorzystane na zmiennej:
- `[]` => tak jak w przypadku tablicy, mamy dostęp do znaków
    ```c++
    std::string tekst = "To jest tekst jest";
    tekst[4] = '$';
    tekst[7] = '$';
    std::cout << tekst << std::endl;
    ```
- `+` lub `+=` => możemy dodawać znaki lub ciągi znaków
    ```c++
    std::string tekst = "To jest tekst jest";
    tekst += '$';
    tekst += "dodajemy wiekszy tekst";
    std::string innyTekst = "cos tam cos tam";
    tekst = tekst + innyTekst;
    tekst += innyTekst;
    std::cout << tekst << std::endl;
    ```

---

Dodatkowe funkcje, które wspomagają pracę typu `std::string`:
- `std::getline({źródło}, {zmienna typu string});` => pobieranie całej linii z źródła (konsola, plik i inne)
    ```c++
    std::string tekst;
    std::getline(std::cin, tekst);
    std::cout << tekst << std::endl;
    ```
- `std::stoi({zmienna typu string});` => konwertuje tekst do liczby całkowitej
- `std::stof({zmienna typu string});` => konwertuje tekst do liczby zmiennoprzecinkowej 1 precyzji
- `std::to_string({zmienna typu int, float});` => konwertuje liczby na tekst