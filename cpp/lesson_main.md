# Główna funkcja programu (main)
```c++
int main(int argc, char* argv[])
{
    return 0;
}
```

Główna funkcja programu niczym się nie różni od zwykłych funkcji, jedynie zawsze posiada 2 argumenty,
które informują nas o:
- `argc` => rozmiar tablicy argv
- `argv` => jest to tablica, która zawiera elementy, które składają się z ciągów znaków
