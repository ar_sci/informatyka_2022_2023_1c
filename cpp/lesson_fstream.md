# Projektowanie oprogramowania: `std::fstream`

`std::fstream` jest to złożony typ danych, który ma za zadanie obsługiwać (wczytywać/zapisywać) dane, które znajdują się na dysku twardym. 

---

Możliwość wykorzystania typu `std::fstream`, poprzez załączenie pliku źródłowego przy wykorzystaniu deryktywy `#include`

```c++
#include <fstream>
```

---

| Tryb | Opis |
|------|------|
| `std::ios::app` | (append - dopisywanie danych do pliku) Ustawia wewnętrzny wskaźnik zapisu pliku na jego koniec. Plik otwarty w trybie tylko do zapisu. Dane mogą być zapisywane tylko i wyłącznie na końcu pliku. |
| `std::ios::ate` | (at end) Ustawia wewnętrzny wskaźnik pliku na jego koniec w chwili otwarcia pliku. |
| `std::ios::binary` | (binary) Informacja dla kompilatora, aby dane były traktowane jako strumień danych binarnych, a nie jako strumień danych tekstowych. |
| `std::ios::in` | (input - wejście/odczyt) Zezwolenie na odczytywanie danych z pliku. |
| `std::ios::out` | (output - wyjście/zapis) Zezwolenie na zapisywanie danych do pliku. |
| `std::ios::trunc` | (truncate) Zawartość pliku jest tracona, plik jest obcinany do 0 bajtów podczas otwierania. |


| Próba odczytu: | Próba zapisu: |
|------|------|
| Plik nie istnieje na dysku | Nie posiadamy uprawnień pozwalających nam modyfikować plik |
| Nie posiadamy uprawnień odczytu do pliku | Nie posiadamy uprawnień do katalogu w którym chcemy utworzyć plik |
|  | Nośnik, na którym chcemy dokonać zapisu jest tylko do odczytu |

---

Przykład wykorzystania typu `std::fstream`:
- zapis danych:
    ```c++
    // utworzenie zmiennej, która obsługuje plik
    std::fstream plik;
    // otworzenie pliku, relatywna ścieżka od pliku wykonywalnego (.exe)
    // tryb został ustawiony na zapis
    plik.open("./test.txt", std::ios::out);
    // sprawdzenie, czy plik został poprawnie otworzony
    if (plik.good()) {
        // przeniesienie danych do pliku
        plik << "Liczba 1: " << 123 << std::endl;
        plik << "Liczba 2: " << 1.3f << std::endl;
        plik << "Tekst: " << "Ala ma kota." << std::endl;

        // wymuszenie na sterowniku, aby przeniusł dane z buffora na plik
        plik.flush();
    }
    // zamkniecie pliku
    plik.close();
    ```
- odczyt danych:
    ```c++
    // utworzenie zmiennej, która obsługuje plik
    std::fstream plik;
    // otworzenie pliku, relatywna ścieżka od pliku wykonywalnego (.exe)
    // tryb został ustawiony na odczyt
    plik.open("./test.txt", std::ios::in);
    // sprawdzenie, czy plik został poprawnie otworzony
    if (plik.good()) {
        // utworzenie pętli while, której zadaniem, jest sprawdzenie czy plik nie doszedł do końca pliku
        // eof -> end of file
        while (!plik.eof()) {
            // utworzenie zmiennej pomocniczej, std::string
            std::string linia;
            // pobranie linii z źródła, std::getline
            std::getline(plik, linia);
            // wyświetlenie linii za pomocą std::cout
            std::cout << linia << std::endl;
        }
    }
    // zamkniecie pliku
    plik.close();
    ```

---

Metody, które mogą zostać wykonane na zmiennej (te najbardziej potrzebne):
- `.good()` => Zwraca wartość true, jeśli ostatnia operacja wejścia/wyjścia na strumieniu zakończyła się powodzeniem.
- `.eof()` => Zwraca wartość true, jeśli powiązany strumień osiągnął koniec pliku.
- `.open(const char *filename, ios_base::openmode mode = ios_base::in|ios_base::out)` => Otwiera i kojarzy plik o nazwie filename ze strumieniem plików.
- `.open(const std::string &filename, ios_base::openmode mode = ios_base::in|ios_base::out );` => Otwiera i kojarzy plik o nazwie filename ze strumieniem plików.
- `.is_open()` => Sprawdza, czy strumień plików ma otwarty plik, który może zostać obsłużony. 
- `.read(char_type* buffor, std::streamsize count)` => metoda pobiera liczbę znaków do bufforu\
    - buffor jest to tablica znaków, do której znaki są pobierane
    - count jest to liczba znaków, która określa liczbę znaków do odczytanych z pliku
    ```c++
    std::fstream plik;
    plik.open("./test.txt", std::ios::in);
    if (plik.good()) {
        char buffor[20];
        plik.get(buffor, 20);
    }
    plik.close();
    ```
- `.write(const char_type* buffor, std::streamsize count)` => metoda zapisuje określoną liczbę znaków z bufforu\
    - buffor jest to tablica znaków, z której są pobierane znaki
    - count jest to liczba znaków, która określa liczbę znaków do zapisania w pliku
    ```c++
    std::fstream plik;
    plik.open("./test.txt", std::ios::out);
    if (plik.good()) {
        const char buffor[6] = "abcde";
        plik.write(buffor, 6);
        plik.flush();
    }
    plik.close();
    ```
- `.close()` => Zamyka powiązany plik.
- `.tellg()` => Zwraca wskaźnik (indeks) pozycji wejściowej bieżącego powiązanego obiektu plikowego. Jest to liczba całkowita. Porównać sobie z działaniem karetki w edytorach tekstowych, czyli zwraca pozycję tej karetki (migający kursor w edytorach tekstowych).
- `.seekg(pos_type pos)` => Ustawia wskaźnik pozycji wejściowej bieżącego powiązanego obiektu plikowego w określone miejsce za pomocą `pos`. Jest to liczba całkowita.
    - pos - pozycja bezwzględna, na którą ma być ustawiony wskaźnik pozycji wejściowej. 
- `.seekg(off_type off, std::ios_base::seekdir dir)` => Ustawia wskaźnik pozycji wejściowej bieżącego powiązanego obiektu plikowego.
    - off - pozycja względna (dodatnia lub ujemna), na którą ma być ustawiony wskaźnik pozycji wejściowej.
    - dir - definiuje pozycję bazową, do której ma być zastosowane przesunięcie względne. Może to być jedna z następujących stałych:
        - beg -> początek strumienia
        - end -> zakończenie strumienia
        - cur -> bieżąca pozycja wskaźnika pozycji strumienia 
    ```c++
    std::fstream plik;
    plik.open("./test.txt", std::ios::in);
    if (plik.good()) {
        plik.seekg(0, std::fstream::end);
        std::streamoff length = plik.tellg();
        plik.seekg (0, std::fstream::beg);

        std::cout << "Plik posiada: " << length << " znakow." << std::endl;
    }
    plik.close();
    ```
---

Operatory, które mogą zostać wykorzystane na zmiennej:
- `<<` => dodaje dane do powiązanego pliku, tak aby zapisać je do pliku (strumień Musi być otwarty w trybie zapisu)
- `>>` => wczytuje znaki z pliku do napotkanej spacji (strumień Musi być otwarty w trybie odczytu)

---

Źródło: [link - cpp0x](https://cpp0x.pl/kursy/Kurs-C++/Dodatkowe-materialy/Obsluga-plikow/305) \
Źrodło: [link - cppreference](https://en.cppreference.com/w/cpp/io/basic_fstream)
