﻿#include <iostream>
#include <string>
#include <vector>

// preprocessing
// skladnia
// kompilacja -> obj
// LINKER -> EXE

namespace sci {
    void foo() {}
    void boo() {}
}

namespace std {
    struct Nazwa
    {
        int a;
        float b;
        std::string c;
    };
}

int func1(int a, int b);
void display(
    const std::Nazwa& nazwa,
    const std::string& text1,
    std::string& text2,
    const std::string& text3
);

namespace sci {
    void roo();
}

int main(int argc, char* argv[])
{
    std::string a;

    sci::roo();

    // modifiers => const / unsigned / signed
    // base      => bool double float int char (long, short)
    // complex   => std::string, std::vector, std::fstream
    std::Nazwa zmienna;
    zmienna.a = 1;

    std::string text = "asdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfasasdasfas";
    display(zmienna, text, text, text);

    const unsigned int number = -1;
    std::cout << number << std::endl;

    //std::cin >> number;

    int wynik = func1(number, 10);

    return 0;
}

namespace sci {
    void roo()
    {
    }
}

int func1(int a, int b)
{
    int val = 10;
    val = val + a * b;
    return val;
}

void display(
    const std::Nazwa& nazwa,
    const std::string& text1,
    std::string& text2,
    const std::string& text3
)
{
    std::cout << text1 << std::endl;

    text2 = "ref";
    std::cout << text2 << std::endl;
    std::cout << text3 << std::endl;
    std::cout << std::endl;
}
