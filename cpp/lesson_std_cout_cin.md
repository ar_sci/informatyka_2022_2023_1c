# Obsługa strumienia wyjściowego i wejściowego
Przekazywanie danych na konsolę.
---

Użyj dyrektywy `include`, aby dodać zawartość pliku o nazwie `iostream` do programu źródłowego.
```c++
#include <iostream>
```

---

## Wyświetlenie informacji na konsolę
```c++
std::cout << "To jest tekst" << std::endl;
int zmienna = 0;
std::cout << zmienna << ' ' << "To jest wartosc" << std::endl;
```

---

## Pobieranie informacji od użytkownika
```c++
int zmienna;
std::cin >> zmienna;

char buffor[255];
std::cin >> buffor;
```
