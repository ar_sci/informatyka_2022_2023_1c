#include <GL/glew.h>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

int main(int /*argc*/, char* /*argv*/[])
{
    sf::ContextSettings settings;
    settings.depthBits = 24;
    settings.stencilBits = 8;
    settings.antialiasingLevel = 4;
    settings.majorVersion = 3;
    settings.minorVersion = 0;

    sf::Window window {
        sf::VideoMode{
            1280,
            720,
        },
        sf::String{ "sci_pi_opengl" },
        sf::Style::Default,
        settings,
    };

    glewInit();

    bool is_running = true;
    sf::Clock clock;
    do {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (sf::Event::EventType::Closed == event.type) {
                is_running = false;
            }
        }

        const sf::Time delta_time = clock.restart();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        window.display();
    } while (is_running);

    return 0;
}
