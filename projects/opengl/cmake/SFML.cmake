include(ExternalProject)

ExternalProject_Add(SFML
    PREFIX third_party
    GIT_REPOSITORY "https://github.com/SFML/SFML.git"
    GIT_TAG "2.5.1"
    CMAKE_ARGS -DSFML_BUILD_AUDIO=OFF -DSFML_BUILD_NETWORK=OFF
    INSTALL_COMMAND ""
    UPDATE_COMMAND ""
)

ExternalProject_Add_Step(SFML
    CopyDynamicLibraries
    DEPENDEES install
    COMMAND ${CMAKE_COMMAND} -E copy_if_different "${CMAKE_BINARY_DIR}/third_party/src/SFML-build/lib/\$(Configuration)/$<IF:$<CONFIG:Debug>,sfml-system-d-2.dll,sfml-system-2.dll>" "${CMAKE_BINARY_DIR}/\$(Configuration)/$<IF:$<CONFIG:Debug>,sfml-system-d-2.dll,sfml-system-2.dll>"
    COMMAND ${CMAKE_COMMAND} -E copy_if_different "${CMAKE_BINARY_DIR}/third_party/src/SFML-build/lib/\$(Configuration)/$<IF:$<CONFIG:Debug>,sfml-window-d-2.dll,sfml-window-2.dll>" "${CMAKE_BINARY_DIR}/\$(Configuration)/$<IF:$<CONFIG:Debug>,sfml-window-d-2.dll,sfml-window-2.dll>"
    COMMAND ${CMAKE_COMMAND} -E copy_if_different "${CMAKE_BINARY_DIR}/third_party/src/SFML-build/lib/\$(Configuration)/$<IF:$<CONFIG:Debug>,sfml-graphics-d-2.dll,sfml-graphics-2.dll>" "${CMAKE_BINARY_DIR}/\$(Configuration)/$<IF:$<CONFIG:Debug>,sfml-graphics-d-2.dll,sfml-graphics-2.dll>"
)

set(SCI_PI_OPENGL_SFML_INCLUDE_DIR "${CMAKE_BINARY_DIR}/third_party/src/SFML/include")
set(SCI_PI_OPENGL_SFML_LIBRARY_DIR "${CMAKE_BINARY_DIR}/third_party/src/SFML-build/lib")
set(SCI_PI_OPENGL_SFML_LIBRARIES
    "$<IF:$<CONFIG:Debug>,sfml-main-d.lib,sfml-main.lib>"
    "$<IF:$<CONFIG:Debug>,sfml-system-d.lib,sfml-system.lib>"
    "$<IF:$<CONFIG:Debug>,sfml-window-d.lib,sfml-window.lib>"
    "$<IF:$<CONFIG:Debug>,sfml-graphics-d.lib,sfml-graphics.lib>"
)
