include(ExternalProject)

ExternalProject_Add(GLEW
    PREFIX third_party
    GIT_REPOSITORY "https://github.com/Perlmint/glew-cmake.git"
    INSTALL_COMMAND ""
    UPDATE_COMMAND ""
)

ExternalProject_Add_Step(GLEW
    CopyDynamicLibraries
    DEPENDEES install
    COMMAND ${CMAKE_COMMAND} -E copy_if_different "${CMAKE_BINARY_DIR}/third_party/src/GLEW-build/bin/\$(Configuration)/$<IF:$<CONFIG:Debug>,glew-sharedd.dll,glew-shared.dll>" "${CMAKE_BINARY_DIR}/\$(Configuration)/$<IF:$<CONFIG:Debug>,glew-sharedd.dll,glew-shared.dll>"
)

set(SCI_PI_OPENGL_GLEW_INCLUDE_DIR "${CMAKE_BINARY_DIR}/third_party/src/GLEW/include")
set(SCI_PI_OPENGL_GLEW_LIBRARY_DIR "${CMAKE_BINARY_DIR}/third_party/src/GLEW-build/lib")
set(SCI_PI_OPENGL_GLEW_LIBRARIES
    "$<IF:$<CONFIG:Debug>,glewd.lib,glew.lib>"
    "$<IF:$<CONFIG:Debug>,glew-sharedd.lib,glew-shared.lib>"
)