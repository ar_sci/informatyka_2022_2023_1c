# Projektowanie oprogramowania: zadania_05 (szyfrowanie / deszyfrowanie)
## Zadanie jest obowiązkowe

***

Wykonaj następujące zadania:

- [ ] Zadanie_0: Implementacja funkcji, która sprawdzi, czy znak znajduje się w zakresie.
- [ ] Zadanie_1: Implementacja funkcji, która sprawdzi, czy znak jest znakiem specjalnym.
- [ ] Zadanie_2: Implementacja funkcji, która sprawdzi, czy ciąg znaków jest prawidłowy.
- [ ] Zadanie_3: Implementacja funkcji szyfrującej.
- [ ] Zadanie_4: Implementacja funkcji deszyfrującej.
- [ ] Zadanie_5: Implementacja programu.

- [ ] Zaimplementuj obsługę małych liter.
- [ ] Zaimplementuj interfejs użytkownika:
    - wybór opcji: szyfrowanie, deszyfrowanie
    - wprowadzenie przesunięcia oraz tekstu
- [ ] Zaimplementuj obsługę liczb.

***

## Linki:
- [szyfr cezara - wikipedia polska](https://pl.wikipedia.org/wiki/Szyfr_Cezara)
- [szyfr cezara - wikipedia angielska](https://en.wikipedia.org/wiki/Caesar_cipher)
- [tablica ASCII](https://jbwyatt.com/I/asciiGood.gif)

## Założenia:
- tekst musi składać się z dużych liter
- tekst nie może zawierać znaków specjalnych (wyjątkiem jest znak spacji lub znak nowej linii)

## Zadanie_0 - szczegóły
- nazwa funkcji: `is_upper_case`
- musi zwracać wartości: `true` lub `false`
- musi przyjmować argument o nazwie `character`, jest to znak
- implementacja polega na sprawdzeniu, czy argument znajduje się w następującym zakresie: `'A' <= character <= 'Z'`
- implementację, możesz sprawdzić za pomocą poniższego kodu w funkcji `main`:
    ```c++
    if (is_upper_case('P')) {
        std::cout << "is_upper_case => OK" << std::endl;
    }
    if (!is_upper_case('1')) {
        std::cout << "is_upper_case => OK" << std::endl;
    }
    ```
- zacommituj zmiany

## Zadanie_1 - szczegóły
- nazwa funkcji: `is_special_character`
- musi zwracać wartości: `true` lub `false`
- musi przyjmować argument o nazwie `character`, jest to znak
- implementacja polega na sprawdzeniu, czy argument jest znakiem spacji (`' '`) lub znakiem nowej linii (`'\n'`)
- implementację, możesz sprawdzić za pomocą poniższego kodu w funkcji `main`:
    ```c++
    if (is_special_character(' ') && is_special_character('\n')) {
        std::cout << "is_special_character => OK" << std::endl;
    }
    if (!is_special_character('G')) {
        std::cout << "is_special_character => OK" << std::endl;
    }
    ```
- zacommituj zmiany

## Zadanie_2 - szczegóły
- nazwa funkcji: `is_string_valid`
- musi zwracać wartości: `true` lub `false`
- musi przyjmować argument o nazwie `str`, jest to tablica znaków / ciąg znaków (`const char *`)
- implementacja polega na napisaniu pętli `for`, która ocenii, czy znaki są prawidłowe
- długość ciagu znaków otrzymasz poprzez wywołanie funkcji `strlen`
- wykorzystaj napisane funkcje do wyrażeń warunkowych `if`
- jeśli natrafisz na znak specjalny, użyj słowa kluczowego `continue`, aby pominąć iterację
- jeśli znak nie znajduje się w zakresie, zwróć `false` za pomocą słowa kluczowego `return`
- jeśli pętla się zakończy, to zwróć wartość `true` na końcu funkcji
- implementację, możesz sprawdzić za pomocą poniższego kodu w funkcji `main`:
    ```c++
    if (is_string_valid("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG")) {
        std::cout << "is_string_valid => OK" << std::endl;
    }
    if (!is_string_valid("THE 1234 BROWN FOX 5123 OVER THE LAZY DOG")) {
        std::cout << "is_string_valid => OK" << std::endl;
    }
    ```
- zacommituj zmiany


## Zadanie_3 - szczegóły
- nazwa funkcji: `encryption`
- funkcja nic nie zwraca: `void`
- musi przyjmować argumenty o nazwie:
    - `str`, jest to tablica znaków / ciąg znaków (`const char *`)
    - `shift`, jest to zmienna informująca o przesunięciu (int)
- implementacja polega na napisaniu pętli `for`, która zaszyfruje tekst, który został przekazany jako parametr
- pętla musi omijać znaki specjalne, za pomocą funkcji `is_special_character`
- następnie zaimplementuj algorymt, który znajduje się na angielskiej wikipedii
    - E(x) = (x + shift) % 26
    - pamiętaj, że x jest znakiem, który powinien zostać ograniczony do zakresu [0, 26)
- zacommituj zmiany

## Zadanie_4 - szczegóły
- nazwa funkcji: `decryption`
- funkcja nic nie zwraca: `void`
- musi przyjmować argumenty o nazwie:
    - `str`, jest to tablica znaków / ciąg znaków (`const char *`)
    - `shift`, jest to zmienna informująca o przesunięciu (int)
- implementacja polega na napisaniu pętli `for`, która deszyfruje tekst, który został przekazany jako parametr
- pętla musi omijać znaki specjalne, za pomocą funkcji `is_special_character`
- następnie zaimplementuj algorymt, który znajduje się na angielskiej wikipedii
    - D(x) = (x - shift) % 26
    - pamiętaj, że x jest znakiem, który powinien zostać ograniczony do zakresu [0, 26)
    - w tym przypadku modulo (%) nie oznacza dzielenia z resztą tylko następujące działania:
        - jeśli wynik `(x - shift) < 0 ` to (x - shift) + 26
        - jeśli wynik `(x - shift) > 26` to (x - shift) - 26
- zacommituj zmiany

***

```c++
std::string = "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG";
encryption(text, 23);
std::cout << text << std::endl;
decryption(text, 23);
std::cout << text << std::endl;
```

Zaszyfrowany: `QEB NRFZH YOLTK CLU GRJMP LSBO QEB IXWV ALD`\
Odszyfrowany: `THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG`

***

## Zadanie_5 - szczegóły
- w funkcji `main` wykorzystaj funkcje `is_string_valid`, aby sprawdzić, czy tekst jest poprawny przed szyfrowaniem oraz deszyfrowaniem. Jeśli nie, wyświetl opdpowiedni komunikat.
- zacommituj zmiany