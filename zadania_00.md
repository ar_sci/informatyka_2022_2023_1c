# Informatyka: zadania_00 (bash scripts)

***

Lista zadań:
- [ ] Zadanie_0: Konwersja minut na sekundy.
- [ ] Zadanie_1: Czy rok jest przestępny.
- [ ] Zadanie_2: Zwracaj liczby negatywne.
- [ ] Zadanie_3: Suma kątów wielokąta.
- [ ] Zadanie_4: Wracasz do domu?
- [ ] Zadanie_5: Mix.

***

## Zadanie_0 - szczegóły
- napisz skrypt, który konwertuje minuty na sekundy
- pobiera dane za pomocą wczytania informacji z terminala lub jako argument skryptu
- oblicza wynik
- wyświetla następujący tekst: `{x} minut to {y} sekund.`

## Zadanie_1 - szczegóły
- napisz skrypt, który sprawdza czy podany rok jest przestępny
- pobiera dane za pomocą wczytania informacji z terminala lub jako argument skryptu
- oblicza wynik
- wyświetla następujący tekst: `2000 jest rokiem przestępnym.` lub `1998 nie jest rokiem przestępnym.`

## Zadanie_2 - szczegóły
- napisz skrypt, który zwraca zawsze liczby ujemne (oprócz zera)
- pobiera dane za pomocą wczytania informacji z terminala lub jako argument skryptu
- oblicza wynik
- wyświetla wynik

## Zadanie_3 - szczegóły
- napisz skrypt, który zlicza sumę kątów wielokąta
- biorąc pod uwagę wielokąt foremny `n`, zwróć całkowitą sumę kątów wewnętrznych (w stopniach)
- `n`jest wczytane za pomocą terminala lub jako argument skryptu
- Uwagi:
    - n zawsze będzie większy niż 2
    - Wzór (n - 2) x 180 daje sumę wszystkich miar kątów n wielokąta

## Zadanie_4 - szczegóły
- Paweł rozpoczął swoją podróż od domu. Biorąc pod uwagę ciąg kierunków (N=Północ, W=Zachód, S=Południe, E=Wschód), będzie szedł przez jedną minutę w każdym kierunku. Ustalcie, czy zestaw kierunków doprowadzi go z powrotem do pozycji wyjściowej, czy też nie.
    - evaluate("EEWE") ➞ false
    - evaluate("NENESSWW") ➞ true
    - evaluate("NEESSW") ➞ false
- napisz skrypt, który wyświetla odpowiedni komunikat

```sh
stringVar="abcde"
for ((i=1;i<=${#stringVar};i++)); do
  echo ${stringVar:$i-1:1}
done
```

## Zadanie_5 - szczegóły
- napisz skrypt, który wykorzysta następujące komendy:
    - mkdir
    - rm -rf
    - touch
    - echo
    - cd
    - read
    - wyrażenia warunkowe `if`
    - pętlę `for`
- im bardziej skomplikowane wykorzystanie powyższych komend tym lepiej

---

