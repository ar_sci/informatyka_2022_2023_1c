# Informatyka: zadania_06

***

## Zadanie_0 - Wejście/Wyjście
- utwórz 3 zmienne, które będą określały następujące dane wejściowe:
    - 2 liczy całkowite (`int`)
    - 1 znak (`char`)
- wyświetl napis: `Wpisz dzialanie: ` (bez końcowej linii)
- za pomocą `std::cin` użyj trzykrotnie operatora `>>`, aby wprowadzić dane do zmiennych (liczba, literka, liczba)
- dane, które użytkownik będzie wprowadzał są następujące:
    - `2 + 2`
    - `53 * 4`
    - `27 % 3`
    - `88 - 28`
    - `57 % 0`

## Zadanie_1 - Funkcja obliczająca wyrażenie matematyczne
- utwórz deklarację funkcji, która zwraca wartość/wynik (`int`) oraz przyjmuje 3 argumenty (2 liczby i znak)
- utworz definicje funkcji, która wylicza odpowiednie działanie matematyczne na podstawie znaku (użyj wyrażeń warunkowych za pomocą `if` lub `switch`)
- funkcja nie ma sprawdzać przy dzieleniu modulo czy dzielnik jest 0 lub ujemny

## Zadanie_2 - Wykorzystanie funkcji
- w funkcji `main` należy sprawdzić, czy znak jest równy `%`, jeśli tak, to należy sprawdzić czy wartość podana jako drugi komponent jest większa niż 0
- jeśli nie to wyświetl odpowiedni komunikat
- w innym przypadku należy wywołać funkcję, przekazując odpowiednie parametry do funkcji
- wynik należy wyświetlić w następującym formacie: `Wynik dzialania wynosi: {liczba}`

## Zadanie_3 - Utworzenie zmiennej obsługującej plik oraz wczytanie pliku
- należy dodać plik nagłówkowy `fstream`, który pozwoli na wczytanie lub zapis danych z/do pliku tekstowego
- utwórz dwie zmiennne wykorzystując typ danych `std::fstream`, jedna zmienna będzie służyła do odczytu danych, druga zaś do zapisu
- na obu zmiennych wywołaj metodę, która nazywa się `open`, wykorzystaj operator dostępu, wypisując nazwę zmiennej oraz `.`
- dla zmiennej od odczytu, wykorzystaj tryb odczytu: `std::ios::in`
- dla zmiennej od zapisu, wykorzystaj tryb odczytu: `std::ios::out`
- pamiętaj, że pierwszym parametrem jest nazwa pliku: `data_input.txt` lub `data_output.txt`
- utwórz wyrażenie logiczne, które sprawdzi, czy pliki są wczytane, wykorzystaj metodę: `is_open`
    - pamiętaj, że możesz połączyć dwa warunki za pomocą operacji logicznej AND, operator: `&&`
- za wyrażeniem warunkowym, należy zamknąć pliki wykorzystując metodę: `close`

## Zadanie_4 - Wczytanie/Zapisanie danych z pliku tekstowego
- pierwsza linia pliku tekstowego oznacza ile działań matematycznych znajduje się w pliku
- tak samo, jak w przypadku wczytywania danych z terminala, możesz wczytywać dane z pliku tekstowego
- wykorzystaj zmienną, która odpowiada za obsługę pliku związanego z danymi wejściowymi, a następnie wykorzystaj operator `>>`, który pozwoli tobie na wczytanie pierwszej linii
- następnie utwórz pętlę for, która umożliwi na wczytanie pozostałych linii
- wczytanie kilku elementów w linii polega na wykorzystaniu zmiennych, do których można przypisać nowe wartości z pliku
    - PS. tak samo jak w przypadku `std::cin` możesz wczytać kilka wartości naraz
- wykorzystaj funkcję aby obliczyć wartość działania matematycznego
- wynik zapisz do pliku odpowiedającego za dane wyjściowe
    - PS. zapisywanie do pliku odbywa się w taki sam sposób co wyświetlanie informacji na konsoli
    - PS. działa również `std::endl`, który doda znak nowej linii

***

### Dane wejściowe i odpowiedzi znajdziesz w katalogu assets