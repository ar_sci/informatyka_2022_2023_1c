# Informatyka: zadania_04

***

Wykonaj następujące zadania:

- [ ] Zadanie_0: Tworzenie tablicy i wypełnianie jej pseudolosowymi wartościami.
- [ ] Zadanie_1: Pętla tworząca sumę.
- [ ] Zadanie_2: Wyszukaj i zlicz.
- [ ] Zadanie_3: Wyszukaj największą oraz najmniejszą liczbę.

***

## Zadanie_0 - szczegóły
- W funkcji main zainicjalizuj zmienną o typie `int`, nazwa `size`, musi być stała `const`, i przypisz do niej wartość `1000`;
- W funkcji main zaimplementuj tablicę o nazwie `array`. Elementy tablicy są typu `int`. Jako rozmiar tablicy, wykorzystaj zmienną, która została utworzona w poprzednim kroku.
- Wypełnij tablicę wartościami `-1` a co trzeci element tablicy wypełnij ją wartością losową. Użyj do tego funkcji `rand` oraz pamiętaj o utworzeniu ziarna na starcie programu, przy pomocy funkcji `srand`. Ustaw następujące ziarno 2137.
- Początkowe indeksy: 0, 3, 6, 9, 12, 15 ...
- Tablica: [7017, -1, -1, 6696, -1, -1, 22252, -1, -1, ...]

## Zadanie_1 - szczegóły
- Zainicjalizuj zmienną o typie `int` o nazwie `sum`, która na początku będzie posiadać wartość `0`
- Zaimplementuj pętlę, która obliczy sumę wszystkich elementów w tablicy.
- Wynik obliczeń wyświetl na ekranie konsoli.

## Zadanie_2 - szczegóły
- Zainizjalizuj zmieną o typie `int` o nazwie `count`, wartośc początkowa to 0.
- Wyszukaj oraz zlicz wszystkie liczby które znajdują się w przedziale między 6776 <= x <= 10101.
- Wynik zliczania wyświetl na ekranie konsoli.

## Zadanie_3 - szczegóły
- Zainizjalizuj dwie zmienne, które będą określać najmniejszą oraz największą liczbę.
- Wyszukaj największą liczbę, która występuje w tablicy.
- Wyszukaj najmniejszą liczbę, która znajduje się w tablicy, proszę nie uwzględniać liczby -1.
- Wyniki wyświetl na ekranie konsoli.

## Odpowiedzi:
- Wynik zadania_1 to: 5584888
- Wynik zadania_2 to: 34
- Wynik zadania_3 to:
    - min: 58
    - max: 32722
