# Informatyka: zadania_07

## Zadania
```
Napisz program w języku programowania C++:
- [ ] Zadanie_1: Realizacja instrukcji z pliku tekstowego.
```

## Zadanie_0 - szczegóły
- W pliku znajduje się 2000 instrukcji, które mają zostać wykonane przez twój program komputerowy.
    - Zestaw instrukcji:
        | Instrukcja | Opis |
        |---|---|
        | DOPISZ *litera* | oznacza, że na końcu napisu trzeba dopisać pojedynczą literę |
        | ZMIEN *litera* | oznacza, że ostatnią literę aktualnego napisu należy zmienić na podaną literę (możesz założyć, że napis jest niepusty) |
        | USUN 1 | oznacza, że  należy  usunąć  ostatnią  literę  aktualnego  napisu  (możesz założyć, że napis jest niepusty) |
        | PRZESUN *litera* | oznacza, że pierwsze od lewej wystąpienie podanej litery w napisie należy zamienić na następną literę w alfabecie (jeśli litera to A, to należy zamienić na B, jeśli B, to na C itd.) Literę Z należy zamienić na A. Jeśli litera nie występuje w napisie, nie należy nic robić |
    - *litera* => są to wielkie litery alfabetu angielskiego

- Napisz program, który wczytuje plik o nazwie `instrukcje.txt`, który znajduje się w katalogu assets.
    - punkt 1: Oblicz  całkowitą  długość  napisu  po  wykonaniu  wszystkich  instrukcji  z  pliku `instrukcje.txt`.
        - Dla pliku `przyklad.txt` długością napisu jest liczba 10.
    - punkt 2: Znajdź najdłuższy ciąg występujących kolejno po sobie instrukcji tego samego rodzaju. Jako  odpowiedź podaj rodzaj instrukcji oraz długość tego ciągu. Istnieje tylko jeden taki ciąg.
        - Dla pliku `przyklad.txt` odpowiedzią jest: rodzaj instrukcji – DOPISZ, długość ciągu – 5. 
    - punkt 3: Oblicz, która litera jest najczęściej dopisywana (najczęściej występuje w instrukcji DOPISZ). Podaj tę literę oraz ile razy jest dopisywana. Istnieje tylko jedna taka litera. 
        - Dla pliku `przyklad.txt` odpowiedzią jest litera U, dopisywana 3 razy. 
    - punkt 4: Podaj napis, który powstanie po wykonaniu wszystkich instrukcji z pliku `instrukcje.txt`.
        - Dla pliku przyklad.txt wynikiem jest napis ALANTURING. 
    - punkt 5: Program zapisuje wyniki do pliku o nazwie `wyniki.txt`
        - Podpunkt 1: 10
        - Podpunkt 2: rodzaj instrukcji – DOPISZ, długość ciągu – 5
        - Podpunkt 3: litera - U, ilosc - 3
        - Podpunkt 4: ALANTURING

- co jest wam potrzebne do wykonania tego zadania:
    - std::fstream
    - std::string
        - usuwanie ostatniej literki: `{string}.erase({string}.end() - 1);`
        - usuwanie ostatniej literki: `{string}.pop_back();`
    - std::getline
    - pętle `for` i pętle `while`
    - wyrażenia warunkowe `if`
    - talice statyczne
    - zmienne: `int`
