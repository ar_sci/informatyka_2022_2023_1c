# Zestawienie produktów - excel

W pliku `task_11_dane.txt` (w katalogu assets) znajdują się wartości produktów w kolejnych dniach roboczych 2008 roku. W każdym wierszu znajduje się 13 danych: data oraz 12 liczb rzeczywistych (zapisanych z dwoma miejscami po przecinku), które są cenami kolejnych produktów (A, B, C, D, E, F, G, H, I, J, K, L) w danym dniu. Wszystkie dane rozdzielone są średnikami.

```
Przykład:
2008-01-02;50,90;51,62;53,12;58,66;58,96;59,26;48,15;48,17;48,19;50,23;50,23;50,23
2008-01-03;50,42;51,13;52,62;57,92;58,22;58,51;48,04;48,06;48,08;50,07;50,07;50,07
```

Wykorzystując dane zawarte w pliku oraz aplikację biurową `excel` wykonaj poniższe polecenia. Odpowiedzi zapisz w pliku {nazwisko}_{imie}_excel_1.txt, a odpowiedź do każdego podpunktu poprzedź literą oznaczającą ten podpunkt.

### A
Dla każdego z produktów podaj średnią cenę (z całego roku). Wynik zaokrąglij do dwóch miejsc po przecinku.

### B
Podaj liczbę dni, w których ceny wszystkich produktów były wyższe niż 33 zł. Takimi dniami były między innymi dni przedstawione w powyższym przykładzie.

### C
Podaj cenę, która w całym roku wystąpiła najczęściej, i napisz, ile razy wystąpiła.

### D
Dla każdego z dwunastu produktów podaj najniższą i najwyższą cenę, jaką odnotowano w 2008 roku. Sporządź wykres liniowy ilustrujący otrzymane zestawienie. Pamiętaj o prawidłowym i czytelnym opisie wykresu.

---

Plik excel oraz odpowiedzi zacommituj na swoim repozytorium.
